const p = require('playwright');

(async () => {
  for (const t of [p.chromium, p.firefox, p.webkit]) {
    const b = await t.launch();
    const p = await b.newPage();
    console.log(await p.evaluate(() => navigator.userAgent));
    await b.close();
  }
})();
